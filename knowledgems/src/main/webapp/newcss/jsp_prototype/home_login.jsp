<%--
  Created by IntelliJ IDEA.
  User: 1
  Date: 2021/7/18
  Time: 15:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
    <link rel="stylesheet" type="text/css" href="../css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" href="../css/bootstrap-reboot.css">
    <link rel="stylesheet" type="text/css" href="../css/mycss.css">
</head>
<body>
<div class="dx-main">
    <c:import url="_navbar.jsp?menu=index"/>
    <header class="dx-box-1">
        <div class="container">
            <div class="bg-image">
                <img src="../picture/bg-header-2.png" alt="">
                <div style="background-color: rgba(27, 27, 27, .8);"></div>
            </div>

            <div class="row justify-content-center">
                <div class="col-xl-10 col-lg-10 col-md-12 col-sm-12 col-12">
                    <h1 class="mb-30 text-white text-center">团队知识管理</h1>
                    <div class="row">
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2"></div>
                        <div class="col-xl-8 col-lg-8 col-md-8 col-sm-8">
                            <form id="search-form" class="dx-form dx-form-group-inputs">
                                <%--输入框--%>
                                <input type="text" style="display: none;"/>
                                <input type="hidden" name="keyword" id="hkeyword"/>
                                <input id="query" type="text" name="queryStr" class="form-control dx-btn-rd" placeholder="在这里发现你想要的知识"/>
                                <button type="button" class="dx-btn dx-btn-lg dx-btn-r" onclick="searchData(1)"><a>搜&nbsp;&nbsp;索</a></button>
                            </form>
                        </div>
                        <div class="col-xl-2 col-lg-2 col-md-2 col-sm-2"></div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div>
        <div class="dx-box bg-white">
            <div class="container">
                <ul class="dx-links dx-isotope-filter text-center">
                    <li class="active" id="recommendLabel" onclick="getRecommendResourcePage(1)">推荐</li>
                    <li id="newestLabel" onclick="getNewestResourcePage(1)">最新</li>
                    <li id="noticeLabel" onclick="getNoticeResourcePage(1)">公告</li>
                    <li id="myLabel" onclick="getMyResourcePage(1)">我的</li>
                </ul>
            </div>
        </div>
        <div class="dx-separator"></div>
        <div class="dx-box-6 bg-grey-6">
            <div class="container">
                <div class="row vertical-gap md-gap">
                    <div class="col-lg-12">
                        <div class="dx-box dx-box-decorated">
                            <div id="page-data" class="text-center">
                                <div class="dx-table dx-table-default">
                                    <table class="dx-table dx-table-default">
                                        <tbody id="result-column">
                                        <tr>
                                            <th scope="row" style="padding-right: 50px" class="dx-table-topics">
                                                <span style="margin-right: 10px"><a class="font-title-2" href="<%=request.getContextPath()%>/resource/resource?id=${resource.id}" target="_blank">测试测试</a></span>
                                                <span class="font-text-gray"><a class="link-darkgray" href="javascript:void(0);" onclick="deleteSpecialResource(${resource.specType}, ${resource.id})">[移除]</a>&nbsp;&nbsp;&nbsp;</span>
                                                <span class="font-text-gray"><a class="link-darkgray" href="javascript:void(0);" onclick="showRecommendOrder(${resource.id})">[排序 ${resource.specOrder}]</a>&nbsp;</span>
                                                <span style="float:right">
                                        <span class="font-text-gray"><a class="link-darkgray" href="../labels/labels?id=${label.trim()}">标签 / 标签</a>&nbsp;</span>
                                    </span>
                                                <br>
                                                <span class="font-text"
                                                      style="word-break: break-all">测试测试测试测试……</span>
                                                <br>
                                                <span class="font-text-gray"><a href="/myresource?userId=${resource.userId}" class="link-darkgray">测试</a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span class="font-text-gray">2020-1-13 21：00</span>
                                                <span style="float:right">
                                        <span class="font-text-gray"><a class="link-darkgray" href="../groupinfo?groupid=${groupId.trim()}">群组 / 群组</a>&nbsp;</span>
                                    </span>
                                            </th>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table class="dx-table dx-table-default">
                                        <tbody>
                                        <tr>
                                            <th scope="row" style="padding-right: 50px" class="dx-table-topics">
                                                <span style="margin-right: 10px"><a class="font-title-2" href="<%=request.getContextPath()%>/resource/resource?id=${resource.id}" target="_blank">测试测试</a></span>
                                                <span class="font-text-gray"><a class="link-darkgray" href="javascript:void(0);" onclick="deleteSpecialResource(${resource.specType}, ${resource.id})">[移除]</a>&nbsp;&nbsp;&nbsp;</span>
                                                <span class="font-text-gray"><a class="link-darkgray" href="javascript:void(0);" onclick="showRecommendOrder(${resource.id})">[排序 ${resource.specOrder}]</a>&nbsp;</span>
                                                <span style="float:right">
                                        <span class="font-text-gray"><a class="link-darkgray" href="../labels/labels?id=${label.trim()}">标签 / 标签</a>&nbsp;</span>
                                    </span>
                                                <br>
                                                <span class="font-text"
                                                      style="word-break: break-all">测试测试测试测试……</span>
                                                <br>
                                                <span class="font-text-gray"><a href="/myresource?userId=${resource.userId}" class="link-darkgray">测试</a></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                <span class="font-text-gray">2020-1-13 21：00</span>
                                                <span style="float:right">
                                        <span class="font-text-gray"><a class="link-darkgray" href="../groupinfo?groupid=${groupId.trim()}">群组 / 群组</a>&nbsp;</span>
                                    </span>
                                            </th>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="text-center">
                                <ul class="dx-pagination mb-30 mnt-7">
                                    <li class="active dx-pagination-icon">
                                        <a class="dx-pagination">1</a>
                                    </li>
                                    <li class="dx-pagination-icon">
                                        <a class="dx-pagination">2</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        <c:import url="_footer.jsp"/>
</div>
</body>
</html>

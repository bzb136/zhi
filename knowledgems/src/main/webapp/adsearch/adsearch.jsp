<%--
  Created by IntelliJ IDEA.
  User: HUYUZHU
  Date: 2020/10/30
  Time: 17:33
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <c:import url="../template/_header.jsp"/>
    <title>高级搜索-知了[团队知识管理应用]</title>

    <link rel="stylesheet" type="text/css" href="../newcss/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="../newcss/css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" href="../newcss/css/bootstrap-reboot.css">
    <link rel="stylesheet" type="text/css" href="../newcss/css/newCSS.css">
</head>

<body>
<div class="re-main">
    <c:import url="../template/_navbar.jsp?menu=search"/>
    <div class="re-box-6 bg-grey-6 mt-85">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 col-xl-12">
                    <div class="re-box-decorated">
                        <div class="re-box-content">
                            <div class="re-form-group" style="line-height:38px;">
                                <span class="mnt-7 font-2"><strong>&nbsp;搜&nbsp;索&nbsp;</strong></span>
                                <input type="text" class="form-control form-control-style-2" id="query" name="queryStr"
                                       placeholder="在这里发现你想要的知识" value="${searchQueryStr}">
                            </div>
                            <br/>
                            <div class="re-form-group" style="line-height:31px;">
                                <span class="mnt-7 font-2"><strong>&nbsp;群&nbsp;组&nbsp;</strong></span>
                                <div style="line-height:45px;">
                                    <c:forEach var="groupList" items="${groupInfoList}">
                                        <c:choose>
                                            <c:when test="${fn:contains(searchGroupIds, groupList.id)}">
                                                <button id="group${groupList.id}" class="re-btn"
                                                        onclick="groupButton(${groupList.id})">${groupList.groupName}</button>
                                            </c:when>
                                            <c:otherwise>
                                                <button id="group${groupList.id}" class="re-btn re-btn-unselected"
                                                        onclick="groupButton(${groupList.id})">${groupList.groupName}</button>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:forEach>
                                </div>
                            </div>
                            <br/>
                            <div class="re-form-group" style="line-height:38px;">
                                <span class="mnt-7 font-2"><strong>&nbsp;分&nbsp;类&nbsp;</strong></span>
                                <div id="labels"></div>
                            </div>
                            <br/>
                            <div class="re-form-group text-center">
                                <button class="re-btn col-md-2" type="submit" id="adsearch" onclick="adsearch()">
                                    <a class="mnt-7">搜 索</a>
                                </button>
                            </div>
                            <br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- START: Footer -->
    <c:import url="../template/_footer.jsp"/>
    <!-- END: Footer -->
</div>
</body>

<!-- START: Scripts -->
<c:import url="../template/_include_js.jsp"/>
<script>
    var labelList = [];
    var lastQueryStr = "${searchQueryStr}";
    var lastGroupIds = [${searchGroupIds}];
    var lastLabelIds = [${searchLabelIds}];
    var groupIds = "${searchGroupIds}";
    var labelIds = "${searchLabelIds}";
    //加这两个判断是为了后面记录groupIds、labelIds变化时不出问题
    if (groupIds.length!=0){
        groupIds = ',' + groupIds
    }
    if (labelIds.length!=0){
        labelIds = ',' + labelIds
    }
    console.log("lastQueryStr:" + lastQueryStr);
    console.log("lastGroupIds:" + lastGroupIds);
    console.log("lastLabelIds:" + lastLabelIds);

    //页面加载后读取标签并且填入搜索条件
    $(document).ready(function (){
        mount();
    })

    //获取数据
    function mount() {
        $.ajax({
            type: "get",
            url: "/adsearch/getlabel",
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                if (data.code === 200) {
                    labelList = data.result;
                    load(labelList);
                } else
                    message(data.msg, false);
            }, error: function (err) {
                message("添加失败：" + err, false);
            }
        });
    }

    //生成可供点选的标签
    function load(tagList) {
        var htmlStr = "";
        for (var j = 0; j < tagList.length; j++) {
            htmlStr += "<div class=\"col-md-12 row\" style=\"margin-left: -20px\">";
            for (var i = 0; i < tagList[j].length; i++) {
                switch (i) {
                    case 0: {
                        htmlStr += "<div>";
                        htmlStr += "<a class=\"re-btn re-label-header\" disabled=\"disabled\">" + tagList[j][i].name + "</a>";
                        htmlStr += "</div>";
                        htmlStr += "<div class=\"col-md-11\">";
                        break;
                    }
                    default: {
                        var tagid = tagList[j][i].id;
                        var tempGroup = "label" + tagid;
                        var uplevelid = tagList[j][i].uplevelid;
                        if (lastLabelIds.indexOf(parseInt(tagid)) == -1) {
                            htmlStr += "<div class='float-left'>";
                            htmlStr += "<button id=\"" + tempGroup + "\" class=\"re-btn re-label-child re-btn-unselected\" onclick=\"labelButton(" + uplevelid + "," + tagid + ")\">" + tagList[j][i].name + "</button>";
                            htmlStr += "</div>";
                        } else {
                            htmlStr += "<div class='float-left'>";
                            htmlStr += "<button id=\"" + tempGroup + "\" class=\"re-btn re-label-child\" onclick=\"labelButton(" + uplevelid + "," + tagid + ")\">" + tagList[j][i].name + "</button>";
                            htmlStr += "</div>";
                        }
                    }
                }
            }
            htmlStr += "</div>";
            htmlStr += " <br/>";
            htmlStr += "</div>";
        }
        $("#labels").html(htmlStr);
    }

    //点击群组标签后变色、记录
    function groupButton(id) {
        if ($('#group' + id).hasClass('re-btn-unselected')) {
            $('#group' + id).removeClass('re-btn-unselected');
            groupIds = groupIds + ',' + id;
        } else {
            $('#group' + id).addClass('re-btn-unselected');
            groupIds = groupIds.replace(',' + id, '');
        }
        console.log("groupIds: " + groupIds);
    }

    //点击分类标签后变色、记录
    function labelButton(uplevelid, tagid) {
        if ($('#label' + tagid).hasClass('re-btn-unselected')) {
            $('#label' + tagid).removeClass('re-btn-unselected');
            labelIds = labelIds + ',' + tagid;
        } else {
            $('#label' + tagid).addClass('re-btn-unselected');
            labelIds = labelIds.replace(',' + tagid, '');
        }
        console.log("labelIds: " + labelIds);
    }

    //传参到searchController
    function adsearch() {
        var query = $("#query").val().trim();
        console.log(groupIds.split()[0])
        if(groupIds.split("")[0]==','){
            groupIds = groupIds.substring(1);
        }
        if(labelIds.split("")[0]==','){
            labelIds = labelIds.substring(1);
        }
        var times = 1;
        <%
           session.setAttribute("queryStr", request.getParameter("queryStr"));
           //System.out.println("query: "+request.getParameter("queryStr"));
        %>
        window.location.href = "<%=request.getContextPath()%>/search/searchAll?queryStr=" + query + "&groupIds=" + groupIds + "&labelIds=" + labelIds + "&times=" + times;
    }
    $(document).keydown(function (event) {
        if (event.keyCode == 13) {
            var query = $("#query").val().trim();
            var times = 1;
            if(groupIds.split("")[0]==','){
                groupIds = groupIds.substring(1);
            }
            if(labelIds.split("")[0]==','){
                labelIds = labelIds.substring(1);
            }
            <%
               session.setAttribute("queryStr", request.getParameter("queryStr"));
               //System.out.println("query: "+request.getParameter("queryStr"));
            %>
            window.location.href = "<%=request.getContextPath()%>/search/searchAll?queryStr=" + query + "&groupIds=" + groupIds + "&labelIds=" + labelIds + "&times=" + times;
        }
    });
</script>
<!-- END: Scripts -->
</html>

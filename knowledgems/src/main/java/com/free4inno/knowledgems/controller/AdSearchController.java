package com.free4inno.knowledgems.controller;

import com.free4inno.knowledgems.domain.GroupInfo;
import com.free4inno.knowledgems.service.LabelService;
import com.free4inno.knowledgems.service.UserService;
import com.free4inno.knowledgems.utils.Constants;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Author HUYUZHU.
 * Date 2020/11/1 14:16.
 */

@Slf4j
@Controller
@RequestMapping("/adsearch")
public class AdSearchController {

    @Autowired
    private LabelService labelService;

    @Autowired
    private UserService userService;

    @RequestMapping("/adsearch")
    public String adsearch(@RequestParam(value = "searchQueryStr", required = false) String searchQueryStr,
                           @RequestParam(value = "searchGroupIds", required = false) String searchGroupIds,
                           @RequestParam(value = "searchLabelIds", required = false) String searchLabelIds,
                           HttpSession session, Map param) {
        log.info(this.getClass().getName()+"----in----" + "高级搜索(adsearch)" + "----"+session.getAttribute(Constants.USER_ID));
        Integer userId = Integer.parseInt(session.getAttribute(Constants.USER_ID).toString());
        List<GroupInfo> groupInfoList = userService.userGroupInfosAll(userId);
        if (searchQueryStr == null) {
            searchQueryStr = "";
        }
        if (searchGroupIds == null) {
            searchGroupIds = "";
        } else {
            searchGroupIds = searchGroupIds.substring(1, searchGroupIds.length() - 1);
        }
        if (searchLabelIds == null) {
            searchLabelIds = "";
        } else {
            searchLabelIds = searchLabelIds.substring(1, searchLabelIds.length() - 1);
        }
        param.put("groupInfoList", groupInfoList);
        param.put("searchQueryStr", searchQueryStr);
        param.put("searchGroupIds", searchGroupIds);
        param.put("searchLabelIds", searchLabelIds);
        log.info(this.getClass().getName()+"----out----" + "跳转到搜索页面" + "----"+session.getAttribute(Constants.USER_ID));
        return "/adsearch/adsearch";
    }

    @ResponseBody
    @GetMapping("/getlabel")
    public Map<String, Object> getLabels(HttpSession session) {
        log.info(this.getClass().getName()+"----in----" + "获取标签(getlabel)" + "----"+session.getAttribute(Constants.USER_ID));
        Map<String, Object> jsonObject = new HashMap<>();
        List<List<Map<String, String>>> tagList = labelService.getLabels();
        jsonObject.put("code", 200);
        jsonObject.put("msg", "OK");
        jsonObject.put("result", tagList);
        log.info(this.getClass().getName()+"----out----" + "返回获取到的标签列表" + "----"+session.getAttribute(Constants.USER_ID));
        return jsonObject;
    }
}

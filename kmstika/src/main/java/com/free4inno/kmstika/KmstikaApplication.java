package com.free4inno.kmstika;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;


/**
 * Author HUYUZHU.
 * Date 2021/3/26 16:56.
 */

@SpringBootApplication
public class KmstikaApplication {
    public static ConfigurableApplicationContext context;

    public static void main(String[] args) {
        context = SpringApplication.run(KmstikaApplication.class, args);
    }

}

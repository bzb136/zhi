package com.free4inno.kmstika.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import java.sql.Timestamp;

/**
 * Author HUYUZHU.
 * Date 2021/3/26 11:32.
 */

@Entity
@Table(name = "attachment")
@Data
@AllArgsConstructor
public class Attachment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id; //主键

    @Column(name = "resourceId")
    private Integer resourceId; //附件所属资源Id

    @Column(name = "createTime")
    private Timestamp createTime; //上传时间

    @Column(name = "name")
    private String name; //名称

    @Column(name = "url")
    private String url; //地址

    @Column(name = "text", columnDefinition = "LONGTEXT")
    private String text; //解析文本

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private AttachmentEnum status; //解析状态

    @Column(name = "overTime")
    private Timestamp overTime; //解析结束时间

    public Attachment() {

    }

    /**
     * 状态.
     */
    public enum AttachmentEnum {
        READY, LOADING, SUCCESS, FAILED;

        public String toString() {
            switch (this) {
                case READY:
                    return "ready";
                case LOADING:
                    return "loading";
                case SUCCESS:
                    return "success";
                case FAILED:
                    return "failed";
            }
            return super.toString();
        }
    }

}

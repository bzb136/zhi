package com.free4inno.kmstika.utils;

import java.util.regex.Pattern;

/**
 * Author HUYUZHU.
 * Date 2021/3/26 16:08.
 */
public class TikaStringUtils {

    /**
     * 清洗字符串中的空白字符.
     *
     * @param input 输入的字符串
     * @return 清洗后的字符串
     */
    public static String cleanString(String input) {
        if (input == null) {
            return input;
        } else {
            return Pattern.compile("\\s*|\t|\r|\n").matcher(input).replaceAll("");
        }
    }
}
